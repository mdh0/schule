package L5_2_4;

public class L5_2_4_While {
    public static void main(String[] args) {
        int pressenPreis = 150000;
        int abschreibungsWert = pressenPreis/10;
        int abschreibungsJahr = 1;
        while (pressenPreis != 0) {
            pressenPreis -= abschreibungsWert;
            System.out.println("Nach Jahr " + abschreibungsJahr + ": " + pressenPreis);
            abschreibungsJahr++;
        }
        System.out.println("Ende der Abschreibungstabelle");
    }
}
