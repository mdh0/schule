package L5_2_4;

public class L5_2_4_For {
    public static void main(String[] args) {
        int pressenPreis = 150000;
        int verbleibenderPreis = pressenPreis;
        for (int abschreibungsJahr = 1; abschreibungsJahr <= 10; abschreibungsJahr++) {
            verbleibenderPreis -= pressenPreis / 10;
            System.out.println("Nach Jahr " + abschreibungsJahr + ": " + verbleibenderPreis);
        }
        System.out.println("Ende der Abschreibungstabelle");
    }
}
