package L5_2_4.L5_2_4_3;

public class Wirtschaftsgut {
    public final double anschaffungsWert;
    public final int abschreibungsDauer;

    public Wirtschaftsgut(double anschaffungsWert, int abschreibungsDauer) {
        this.anschaffungsWert = anschaffungsWert;
        this.abschreibungsDauer = abschreibungsDauer;
    }

    public void zeigeAbschreibungsverlauf() {
        int restWert = (int)anschaffungsWert;
        for (int jahre = 1; jahre <= abschreibungsDauer; jahre++) {
            restWert -= (anschaffungsWert * (1/(double)abschreibungsDauer));
            System.out.println(jahre + ". Jahr: " + restWert + "€");
        }
    }
}
