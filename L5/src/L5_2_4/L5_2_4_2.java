package L5_2_4;

public class L5_2_4_2 {
    public static void main(String[] args) {
        double anschaffungsWert, restWert;
        anschaffungsWert = restWert = 67900;
        double jahre = 10;
        int restBuchWert = 40000;
        int vergangeneJahre = 0;

        while (restWert > restBuchWert) {
            restWert -= (anschaffungsWert * (1 / jahre));
            vergangeneJahre++;
            System.out.println(vergangeneJahre + ": " + restWert + "€");
        }
        System.out.println(vergangeneJahre + "Jahre");
    }
}
