import java.util.ArrayList;

public class L5_4_2 {
    public static void main(String[] args) {
        ArrayList<String> lieblingsFaecher = new ArrayList<String>();
        lieblingsFaecher.add("Mathe");
        lieblingsFaecher.add("Informatik");
        lieblingsFaecher.add("Anderes Fach");
        System.out.println("Meine Lieblingsfächer sind:");
        for (int counter = 0; counter < lieblingsFaecher.size(); counter++) {
            System.out.println((counter+1) + ". " + lieblingsFaecher.get(counter));
        }
        lieblingsFaecher.set(0, "Leibesübung");
        System.out.println(lieblingsFaecher.get(0));
    }
}
