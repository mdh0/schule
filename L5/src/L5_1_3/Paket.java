package L5_1_3;

public class Paket {
    private double gewicht;
    private String zielOrt = "Kein Zielort definiert";

    public Paket(double gewicht) {
        this.gewicht = gewicht;
    }

    public Paket(double gewicht, String zielOrt) {
        this.gewicht = gewicht;
        this.zielOrt = zielOrt;
    }

    public double getGewicht() {
        return gewicht;
    }

    public String getZielOrt() {
        return zielOrt;
    }

    public void setZielOrt(String zielOrt) {
        this.zielOrt = zielOrt;
    }

    public double berechnePorto() {
        if (gewicht > 1500) {
            return getGewicht() * 0.049;
        }
        else {
            return getGewicht() * 0.08;
        }
    }
}
