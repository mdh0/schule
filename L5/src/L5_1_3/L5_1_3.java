package L5_1_3;

import java.util.Scanner;

public class L5_1_3 {
    static Scanner in = new Scanner(System.in);

    public static void main(String[] args) {
        Paket testPaket = new Paket(1480, "Flensburg");
        System.out.println("Das Porto nach " + testPaket.getZielOrt() + " beträgt: " + testPaket.berechnePorto() + "€");
    }
}
