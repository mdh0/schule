import java.util.InputMismatchException;
import java.util.Scanner;

public class L5_1_1 {
    static int verkaufserloease;
    static float provisionprozent;

    public static void main (String[] args) {
        System.out.println("Bitte Verkaufserlöse angeben (€): ");
        Scanner in = new Scanner(System.in);
        try {
            verkaufserloease = in.nextInt();
        }
        catch (InputMismatchException e) {
            System.out.println("Invalid input");
            System.exit(0);
        }
        if (verkaufserloease >= 50000) {
            provisionprozent = 0.07f;
        }
        else {
            provisionprozent = 0.05f;
        }
        System.out.println("Provsision betträgt: " + verkaufserloease * provisionprozent + "€");
    }
}
