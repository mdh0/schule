package L5_1_6;

public class Main {
    public static void main (String[] args) {
        Busgeldeintrag eintrag = new Busgeldeintrag("Kailer", "Micha", 80, 142);
        System.out.println("Das Bußgeld für " + eintrag.getNachname() + ", " + eintrag.getVorname() + " beträgt: " + Geschwindigkeitsueberschreitung.berechneBusgeld(eintrag.getDrivenSpeed()- eintrag.getAllowedSpeed()) + "€");
        System.out.println(eintrag.getNachname() + ", " + eintrag.getVorname() + " erhält dafür " + Geschwindigkeitsueberschreitung.berechnePunkte(eintrag.getDrivenSpeed()- eintrag.getAllowedSpeed()) + " Punkte.");
    }
}
