package L5_1_6;

public final class Geschwindigkeitsueberschreitung {
    private Geschwindigkeitsueberschreitung() {
        super();
    }

    public static int berechneBusgeld (int ueberschreitung) {
        if (ueberschreitung > 70) {
            return 600;
        }
        else if (ueberschreitung > 60) {
            return 440;
        }
        else if (ueberschreitung > 50) {
            return 240;
        }
        else if (ueberschreitung > 40) {
            return 160;
        }
        else if (ueberschreitung > 30) {
            return 120;
        }
        else if (ueberschreitung > 25) {
            return 80;
        }
        else if (ueberschreitung > 20) {
            return 70;
        }
        else if (ueberschreitung > 15) {
            return 60;
        }
        else if (ueberschreitung > 10) {
            return 40;
        }
        else if (ueberschreitung > 0) {
            return 20;
        }
        else {
            return 0;
        }
    }

    public static int berechnePunkte (int ueberschreitung) {
        if (ueberschreitung > 40) {
            return 2;
        }
        else if (ueberschreitung > 20) {
            return 1;
        }
        else {
            return 0;
        }
    }
}
