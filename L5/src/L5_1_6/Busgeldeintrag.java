package L5_1_6;

public class Busgeldeintrag {
    private String nachname;
    private String vorname;
    private int allowedSpeed;
    private int drivenSpeed;

    public Busgeldeintrag(String nachname, String vorname, int allowedSpeed, int drivenSpeed) {
        this.nachname = nachname;
        this.vorname = vorname;
        this.allowedSpeed = allowedSpeed;
        this.drivenSpeed = drivenSpeed;
    }

    public String getNachname() {
        return nachname;
    }

    public String getVorname() {
        return vorname;
    }

    public int getAllowedSpeed() {
        return allowedSpeed;
    }

    public int getDrivenSpeed() {
        return drivenSpeed;
    }
}
