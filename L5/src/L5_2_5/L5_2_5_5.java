package L5_2_5;

import java.util.Scanner;

public class L5_2_5_5 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Bitte Anlagesumme eingeben:");
        double anlageSumme = input.nextInt();
        System.out.println("Bitte Laufzeit eingeben:");
        int laufZeit = input.nextInt();
        System.out.println("Bitte Zinssatz eingeben:");
        double zinsSatz = input.nextDouble();
        for (int vergangeneJahre = 1; vergangeneJahre <= laufZeit; vergangeneJahre++) {
            anlageSumme *= 1 + zinsSatz;
            System.out.println(vergangeneJahre + ": " + anlageSumme + "€");
        }
    }
}
