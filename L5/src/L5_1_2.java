import java.util.InputMismatchException;
import java.util.Scanner;

public class L5_1_2 {
    static Scanner in = new Scanner(System.in);

    static int naechte;
    static int anzahlPersonen;

    static double einzelPreis;

    public static void main (String[] args) {
        System.out.println("Anzahl der Nächte eingeben:");
        try {
            naechte = in.nextInt();
        }
        catch (InputMismatchException e) {
            System.out.println("Invalider input");
            System.exit(69);
        }
        System.out.println("Anzahl der Personen eingeben:");
        try {
            anzahlPersonen = in.nextInt();
        }
        catch (InputMismatchException e) {
            System.out.println("Invalider input");
            System.exit(69);
        }
        if (anzahlPersonen > 15) {
            einzelPreis = 24.90;
        }
        else {
            einzelPreis = 27.40;
        }
        System.out.println("Der Gesamtpreis der Gruppe beträgt: " + anzahlPersonen * einzelPreis * naechte + "€");
    }
}
