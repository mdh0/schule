public class L5_3_2 {
    public static void main(String[] args) {
        int[] pflanzenGroessen = {45, 190, 245, 59, 590, 15};
        double durchschnitt = 0;
        for (int pflanze : pflanzenGroessen) {
            System.out.println(pflanze + " cm");
            durchschnitt += pflanze;
        }
        durchschnitt /= pflanzenGroessen.length;
        System.out.println("Durchschnitt: " + durchschnitt + " cm");
    }
}
