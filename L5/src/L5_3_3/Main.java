package L5_3_3;

public class Main {
    public static void main(String[] args) {
        Konto konto1 = new Konto(new int[]{2, 2, 3, 4, 9, 0, 4, 104});
        Konto konto2 = new Konto(new int[]{2, 3, 2, 4, 9, 0, 4, 104});
        System.out.println("Ist die Kontonummer für Konto 1 valide?: " + konto1.checkChecksum());
        System.out.println("Ist die Kontonummer für Konto 2 valide?: " + konto2.checkChecksum());
    }
}
