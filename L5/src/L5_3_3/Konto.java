package L5_3_3;

public class Konto {
    private final int[] kontoNummer;

    public Konto(int[] kontoNummer) {
        this.kontoNummer = kontoNummer;
    }

    public int[] getKontoNummer() {
        return kontoNummer;
    }

    public boolean checkChecksum() {
        int checkSum = 0;
        for (int counter = 0; counter < kontoNummer.length-1; counter++) {
            checkSum += kontoNummer[counter] * (counter+1);
        }
        return checkSum == kontoNummer[kontoNummer.length-1];
    }
}
