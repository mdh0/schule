package L5_4_4;

import java.util.ArrayList;

public class Kurs {
    private String kursBezeichnung;
    private int kursGebuehr;
    ArrayList<Teilnehmer> teilnehmer = new ArrayList<>();

    public Kurs(String kursBezeichnung, int kursGebuehr) {
        this.kursBezeichnung = kursBezeichnung;
        this.kursGebuehr = kursGebuehr;
    }

    public String getKursBezeichnung() {
        return kursBezeichnung;
    }

    public void setKursBezeichnung(String kursBezeichnung) {
        this.kursBezeichnung = kursBezeichnung;
    }

    public int getKursGebuehr() {
        return kursGebuehr;
    }

    public void setKursGebuehr(int kursGebuehr) {
        this.kursGebuehr = kursGebuehr;
    }

    public void addTeilnehmer(Teilnehmer teilnehmer) {
        this.teilnehmer.add(teilnehmer);
    }

    public Teilnehmer getTeilnehmer(int index) {
        return teilnehmer.get(index);
    }

    public int getAnzahlTeilnehmer() {
        return teilnehmer.size();
    }
}
