package L5_4_4;

public class Main {
    public static void main(String[] args) {
        Teilnehmer meier = new Teilnehmer("Arnold", "Meier");
        Teilnehmer mueller = new Teilnehmer("Hannah", "Müller");
        Teilnehmer schimmel = new Teilnehmer("Elisabeth", "Schimmel");
        Kurs excel = new Kurs("Excel Grundkurs", 75);
        Kurs taichi = new Kurs("Tai Chi", 75);
        excel.addTeilnehmer(meier);
        excel.addTeilnehmer(mueller);
        taichi.addTeilnehmer(mueller);
        taichi.addTeilnehmer(schimmel);
        System.out.println("Erster Teilnehmer des Excel-Kurses: " + excel.getTeilnehmer(0).getNachname());
        System.out.println("Erster Teilnehmer des Tai Chi-Kurses: " + taichi.getTeilnehmer(1).getNachname());
        System.out.println("Anzahl Teilnehmer im Tai Chi-Kurs: " + taichi.getAnzahlTeilnehmer());
    }
}
