package L5_1_10;

public class L5_1_10_2 {
    public static void main(String[] args) {
        Veranstaltung v1 = new Veranstaltung(600, "Insert Datum", Veranstaltungsart.Fe);
        Veranstaltung v2 = new Veranstaltung(9999, "Gestern", Veranstaltungsart.Ko);
        Veranstaltung v3 = new Veranstaltung(3, "In 10 Jahren", Veranstaltungsart.So);

        System.out.println(v1.berechneHallenmiete());
        System.out.println(v2.berechneHallenmiete());
        System.out.println(v3.berechneHallenmiete());
    }
}

class Veranstaltung {
    private int numberVisitors;
    private String date;
    private Veranstaltungsart type;

    public void setNumberVisitors(int numberVisitors) {
        this.numberVisitors = numberVisitors;
    }

    public int getNumberVisitors() {
        return numberVisitors;
    }

    public String getDate() {
        return date;
    }

    public Veranstaltungsart getType() {
        return type;
    }

    public Veranstaltung(int numberVisitors, String date, Veranstaltungsart type) {
        this.numberVisitors = numberVisitors;
        this.date = date;
        this.type = type;
    }

    public double berechneHallenmiete() {
        if (getType() == Veranstaltungsart.Ko) {
            if (getNumberVisitors() > 500) {
                return numberVisitors * 1.9;
            }
            else {
                return numberVisitors * 2.4;
            }
        }
        else if (getType() == Veranstaltungsart.Th || getType() == Veranstaltungsart.Fe) {
            return numberVisitors * 1.4;
        }
        else {
            return numberVisitors * 1.2;
        }
    }
}

enum Veranstaltungsart {
    Ko, //Konzert
    Th, //Theater
    Fe, //Vereinsfeier
    So  //Sonstige
}