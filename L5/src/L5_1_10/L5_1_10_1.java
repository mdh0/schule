package L5_1_10;

class Auftrag {
    private Status status;
    private double workTime;
    private double materialCost;

    public Auftrag(Status status, double workTime, double materialCost) {
        this.status = status;
        this.workTime = workTime;
        this.materialCost = materialCost;
    }

    public Status getStatus() {
        return status;
    }

    public double getWorkTime() {
        return workTime;
    }

    public double getMaterialCost() {
        return materialCost;
    }

    public void setWorkTime(double workTime) {
        this.workTime = workTime;
    }

    public double kalkuliereAuftragssumme() {
        if (getStatus() == Status.Freund) {
            return (getWorkTime() * 30) + (getMaterialCost() * 1.05);
        }
        else if (getStatus() == Status.Stammkunde && getWorkTime() > 150) {
            return (getWorkTime() * 37) + (getMaterialCost() * 1.2);
        }
        else if (getStatus() == Status.Stammkunde && getWorkTime() <= 150) {
            return (getWorkTime() * 39) + (getMaterialCost() * 1.25);
        }
        else {
            if (getWorkTime() > 150) {
                return (getWorkTime() * 42) + (getMaterialCost() * 1.3);
            }
            else {
                return (getWorkTime() * 44) + (getMaterialCost() * 1.3);
            }
        }
    }
}

public class L5_1_10_1 {
    public static void main(String[] args) {
        Auftrag a1 = new Auftrag(Status.Freund, 50, 300);
        Auftrag a2 = new Auftrag(Status.Freund, 160, 300);
        Auftrag a3 = new Auftrag(Status.Normalkunde, 50, 300);
        Auftrag a4 = new Auftrag(Status.Normalkunde, 160, 300);
        Auftrag a5 = new Auftrag(Status.Stammkunde, 50, 300);
        Auftrag a6 = new Auftrag(Status.Stammkunde, 160, 300);

        System.out.println(a1.kalkuliereAuftragssumme());
        System.out.println(a2.kalkuliereAuftragssumme());
        System.out.println(a3.kalkuliereAuftragssumme());
        System.out.println(a4.kalkuliereAuftragssumme());
        System.out.println(a5.kalkuliereAuftragssumme());
        System.out.println(a6.kalkuliereAuftragssumme());
    }
}

enum Status {
    Stammkunde,
    Freund,
    Normalkunde
}