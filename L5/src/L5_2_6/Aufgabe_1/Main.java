package L5_2_6.Aufgabe_1;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class Main {
    public static void main(String[] args) {
        double koerperGroesse = 1.8;
        System.out.println("Körpergröße: " + koerperGroesse + " m:");
        for (int gewicht = 40; gewicht <= 90; gewicht += 10) {
            double bmi = (double)gewicht / Math.pow(koerperGroesse, 2);
            DecimalFormat df = new DecimalFormat("#.#");
            df.setRoundingMode(RoundingMode.CEILING);
            System.out.println("Gewicht: " + gewicht + " kg -> BMI: " + df.format(bmi));
        }
    }
}
