package L5_2_6.Aufgabe_2;

public class Patient {
    private double koerperGroesse;
    private double gewicht;

    public Patient(double koerperGroesse, double gewicht) {
        this.koerperGroesse = koerperGroesse;
        this.gewicht = gewicht;
    }

    public double getKoerperGroesse() {
        return koerperGroesse;
    }

    public void setKoerperGroesse(double koerperGroesse) {
        this.koerperGroesse = koerperGroesse;
    }

    public double getGewicht() {
        return gewicht;
    }

    public void setGewicht(double gewicht) {
        this.gewicht = gewicht;
    }

    public double ausgebenBmi() {
        return gewicht / Math.pow(koerperGroesse, 2);
    }
}
