package L5_3_5;

public class Main {
    public static void main(String[] args) {
        Dienstfahrzeug pkw1 = new Dienstfahrzeug(21000, 110000);
        Dienstfahrzeug pkw2 = new Dienstfahrzeug(28000, 165000);
        Dienstfahrzeug pkw3 = new Dienstfahrzeug(19000, 120000);
        Dienstfahrzeug pkw4 = new Dienstfahrzeug(21900, 105000);
        Dienstfahrzeug[] pkws = {pkw1, pkw2, pkw3, pkw4};
        int gefahreneKilometer = 0;
        for (int counter = 0; counter < pkws.length; counter++) {
            gefahreneKilometer += pkws[counter].getGefahreneKM();
        }
        System.out.println("Insgesamte gefahrene Kilometer: " + gefahreneKilometer + " km");
        double durchschnittlicherAnschaffungsPreis = 0;
        for (int counter = 0; counter < pkws.length; counter++) {
            durchschnittlicherAnschaffungsPreis += pkws[counter].getAnschaffungsPreis();
        }
        durchschnittlicherAnschaffungsPreis = durchschnittlicherAnschaffungsPreis / pkws.length;
        System.out.println("Durchschnittlicher Anschaffungspreis: " + durchschnittlicherAnschaffungsPreis + "€");
    }
}
