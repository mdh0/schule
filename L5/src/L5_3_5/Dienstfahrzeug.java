package L5_3_5;

public class Dienstfahrzeug {
    private double anschaffungsPreis;
    private int gefahreneKM;

    public Dienstfahrzeug(double anschaffungsPreis, int gefahreneKM) {
        this.anschaffungsPreis = anschaffungsPreis;
        this.gefahreneKM = gefahreneKM;
    }

    public double getAnschaffungsPreis() {
        return anschaffungsPreis;
    }

    public void setAnschaffungsPreis(double anschaffungsPreis) {
        this.anschaffungsPreis = anschaffungsPreis;
    }

    public int getGefahreneKM() {
        return gefahreneKM;
    }

    public void setGefahreneKM(int gefahreneKM) {
        this.gefahreneKM = gefahreneKM;
    }
}
