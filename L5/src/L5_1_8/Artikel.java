package L5_1_8;

public class Artikel {
    private String name;
    private double price;
    private Artikelkürzel kuerzel;

    public Artikel(String name, double price, Artikelkürzel kuerzel) {
        this.name = name;
        this.price = price;
        this.kuerzel = kuerzel;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public Artikelkürzel getKuerzel() {
        return kuerzel;
    }

    public double berechneRabattPreis() {
        if (kuerzel == Artikelkürzel.Le) {
            return price * 0.85;
        }
        else {
            return price * 0.9;
        }
    }
}
