package L5_1_8;

public class Main {
    public static void main (String[] args) {
        Artikel a1 = new Artikel("Schokolade", 1.00, Artikelkürzel.Le);
        Artikel a2 = new Artikel("Shampoo", 5.00, Artikelkürzel.Kp);
        System.out.println("Der rabbatierte Preis für " + a1.getName() + " beträgt: " + a1.berechneRabattPreis() + " €");
        System.out.println("Der rabbatierte Preis für " + a2.getName() + " beträgt: " + a2.berechneRabattPreis() + " €");
    }
}
