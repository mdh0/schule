package L5_1_8;

public enum Artikelkürzel {
    Le, //Lebensmittle
    Kp, //Körperpflege
    Hw, //Haushaltswaren
    Fl, //Fluid, Getränke
    An  //Andere
}
