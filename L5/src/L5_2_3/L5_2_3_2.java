package L5_2_3;

public class L5_2_3_2 {
    public static void main(String[] args) {
        double height = 7;
        while (height < 32) {
            height += 1.5;
            System.out.println(height + " m");
        }
    }
}
