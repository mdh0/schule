package L5_2_3.L5_2_3_4;

public class Anlage {
    private double anlage;
    private double zinssatz;
    private double dauer;   //in Jahren

    public Anlage(double anlage, double zinssatz, double dauer) {
        this.anlage = anlage;
        this.zinssatz = zinssatz;
        this.dauer = dauer;
    }

    public double getAnlage() {
        return anlage;
    }

    public void setZinssatz(double zinssatz) {
        this.zinssatz = zinssatz;
    }

    public void setDauer(double dauer) {
        this.dauer = dauer;
    }

    public double getZinssatz() {
        return zinssatz;
    }

    public double getDauer() {
        return dauer;
    }

    public void berechneAnlagenentwicklung() {
        double tempMoney = getAnlage();
        for (int i = 0; i < getDauer(); i++) {
            tempMoney = tempMoney * (1 + getZinssatz());
            System.out.printf("%.2f €\n", tempMoney);
        }
    }
}
