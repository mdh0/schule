package L5_2_3;

public class L5_2_3_3 {
    public static void main(String[] args) {
        double money = 5000;
        double interest = 0.06;
        double duration = 25;   //in years

        for (int i = 0; i < duration; i++) {
            money = money * (1 + interest);
            System.out.printf("%.2f\n", money);
        }
    }
}
