package L5_1_5;

public class Paket {
    private double gewicht;
    private String zielOrt = "Kein Zielort definiert";

    public Paket(double gewicht) {
        this.gewicht = gewicht;
    }

    public Paket(double gewicht, String zielOrt) {
        this.gewicht = gewicht;
        this.zielOrt = zielOrt;
    }

    public double getGewicht() {
        return gewicht;
    }

    public String getZielOrt() {
        return zielOrt;
    }

    public void setZielOrt(String zielOrt) {
        this.zielOrt = zielOrt;
    }

    public double berechnePorto() {
        if (getGewicht() > 1800) {
            return getGewicht() * 0.035;
        }
        else if (getGewicht() > 1450) {
            return getGewicht() * 0.049;
        }
        else if (getGewicht() > 1200) {
            return getGewicht() * 0.06;
        }
        else {
            return getGewicht() * 0.08;
        }
    }
}
