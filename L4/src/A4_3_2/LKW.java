package A4_3_2;

public class LKW extends Fahrzeug {
    private int maximumGewicht;
    private double laenge;

    public LKW(String kennzeichen, int preis, int maximumGewicht, double laenge) {
        super(kennzeichen, preis);
        this.maximumGewicht = maximumGewicht;
        this.laenge = laenge;
    }

    public int getMaximumGewicht() {
        return maximumGewicht;
    }

    public double getLaenge() {
        return laenge;
    }
}
