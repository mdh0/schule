package A4_3_2;

public abstract class Fahrzeug {
    protected String kennzeichen;
    protected int preis;

    public Fahrzeug(String kennzeichen, int preis) {
        this.kennzeichen = kennzeichen;
        this.preis = preis;
    }

    public String getKennzeichen() {
        return kennzeichen;
    }

    public int getPreis() {
        return preis;
    }
}
