package A4_3_2;

public class PKW extends Fahrzeug {
    private int sitze;

    public PKW(String kennzeichen, int preis, int sitze) {
        super(kennzeichen, preis);
        this.sitze = sitze;
    }

    public int getSitze() {
        return sitze;
    }
}
