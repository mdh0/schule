package A4_6_4;

public class Adresse {
    public final String strasse;
    public final int plz;
    public final String ort;
    public final String name;

    public Adresse(String strasse, int plz, String ort, String name) {
        this.strasse = strasse;
        this.plz = plz;
        this.ort = ort;
        this.name = name;
    }
}
