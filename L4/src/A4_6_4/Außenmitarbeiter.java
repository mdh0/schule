package A4_6_4;

public class Außenmitarbeiter extends Mitarbeiter {
    private double provisionsSatz;

    public Außenmitarbeiter(int personalNummer, String name, int gehalt, double provisionsSatz) {
        super(personalNummer, name, gehalt);
        this.provisionsSatz = provisionsSatz;
    }

    public double getProvisionsSatz() {
        return provisionsSatz;
    }

    public void setProvisionsSatz(double provisionsSatz) {
        this.provisionsSatz = provisionsSatz;
    }

    public double berechneAuszahlung(double umsatz) {
        return gehalt + (umsatz * provisionsSatz);
    }
}
