package A4_6_4;

public class PKV {
    public final String kfzNummer;
    public final String modell;
    private Mitarbeiter aktuellerFahrer;

    public PKV(String kfzNummer, String modell) {
        this.kfzNummer = kfzNummer;
        this.modell = modell;
    }

    public Mitarbeiter getAktuellerFahrer() {
        return aktuellerFahrer;
    }

    public void setAktuellerFahrer(Mitarbeiter aktuellerFahrer) {
        this.aktuellerFahrer = aktuellerFahrer;
    }
}
