package A4_6_4;

public class Geschaeftsleitung extends Mitarbeiter {
    private double gewinnAnteil;

    public Geschaeftsleitung(int personalNummer, String name, int gehalt, double gewinnAnteil) {
        super(personalNummer, name, gehalt);
        this.gewinnAnteil = gewinnAnteil;
    }

    public double getGewinnAnteil() {
        return gewinnAnteil;
    }

    public void setGewinnAnteil(double gewinnAnteil) {
        this.gewinnAnteil = gewinnAnteil;
    }
}
