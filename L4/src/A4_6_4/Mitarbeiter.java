package A4_6_4;

public class Mitarbeiter {
    protected final int personalNummer;
    protected String name;
    protected int gehalt;

    public Mitarbeiter(int personalNummer, String name, int gehalt) {
        this.personalNummer = personalNummer;
        this.name = name;
        this.gehalt = gehalt;
    }

    public int getPersonalNummer() {
        return personalNummer;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGehalt() {
        return gehalt;
    }

    public void setGehalt(int gehalt) {
        this.gehalt = gehalt;
    }

    public double berechneAuszahlung(double umsatz) {
        return gehalt;
    }
}
