package A4_6_4;

public class Main {
    public static void main(String[] args) {
        Standort standort = new Standort(new Adresse("Lustige Straße 1", 12345, "Lustiger Ort", "Lustiger Name"));
        standort.addMitarbeiter(new Außenmitarbeiter(1, "Peter Lustig", 3, 0.05));
        standort.addPKV(new PKV("Lustiges Kennzeichne", "Lustiges Modell"));
        standort.getPkvs().get(1).setAktuellerFahrer(standort.getMitarbeiter().get(1));
        System.out.println("Kennzeichen: " + standort.getPkvs().get(1).kfzNummer + "; Standort: " + standort.adresse.name
                            + "; Fahrer: " + standort.getPkvs().get(1).getAktuellerFahrer().getName() + "; Gehalt :"
                            + standort.getPkvs().get(1).getAktuellerFahrer().berechneAuszahlung(35000));
    }
}
