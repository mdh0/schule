package A4_6_4;

import java.util.List;

public class Standort {
    public final Adresse adresse;
    private List<PKV> _pkvs;
    private List<Mitarbeiter> _mitarbeiter;

    public Standort(Adresse adresse) {
        this.adresse = adresse;
    }

    public List<PKV> getPkvs() {
        return _pkvs;
    }

    public List<Mitarbeiter> getMitarbeiter() {
        return _mitarbeiter;
    }

    public void addMitarbeiter(Mitarbeiter mitarbeiter) {
        _mitarbeiter.add(mitarbeiter);
    }

    public void addPKV(PKV pkv) {
        _pkvs.add(pkv);
    }
}
