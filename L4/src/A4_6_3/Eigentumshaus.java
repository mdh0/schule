package A4_6_3;

public class Eigentumshaus extends Immobilie {
    private int verkaufPreis;
    private final double grundStuecksGroesse;

    public Eigentumshaus(Adresse adresse, int wohnFlaeche, int verkaufPreis, double grundStuecksGroesse) {
        super(adresse, wohnFlaeche);
        this.verkaufPreis = verkaufPreis;
        this.grundStuecksGroesse = grundStuecksGroesse;
    }

    public int getVerkaufPreis() {
        return verkaufPreis;
    }

    public void setVerkaufPreis(int verkaufPreis) {
        this.verkaufPreis = verkaufPreis;
    }

    public double getGrundStuecksGroesse() {
        return grundStuecksGroesse;
    }
}
