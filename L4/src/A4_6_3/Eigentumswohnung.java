package A4_6_3;

public class Eigentumswohnung extends Immobilie {
    private int verkaufsPreis;
    private final int stockWerke;

    public Eigentumswohnung(Adresse adresse, int wohnFlaeche, int verkaufsPreis, double provisionsSatz, int stockWerke) {
        super(adresse, wohnFlaeche);
        this.verkaufsPreis = verkaufsPreis;
        this.stockWerke = stockWerke;
    }

    public int getVerkaufsPreis() {
        return verkaufsPreis;
    }

    public void setVerkaufsPreis(int verkaufsPreis) {
        this.verkaufsPreis = verkaufsPreis;
    }

    public int getStockWerke() {
        return stockWerke;
    }
}
