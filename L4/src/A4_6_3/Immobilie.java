package A4_6_3;

public abstract class Immobilie {
    public final Adresse adresse;
    protected final int wohnFlaeche;
    private static double provisionsSatz;

    public Immobilie(Adresse adresse, int wohnFlaeche) {
        this.adresse = adresse;
        this.wohnFlaeche = wohnFlaeche;
    }

    public int getWohnFlaeche() {
        return wohnFlaeche;
    }

    public static double getProvisionsSatz() {
        return provisionsSatz;
    }

    public static void setProvisionsSatz(double provisionsSatz) {
        Immobilie.provisionsSatz = provisionsSatz;
    }
}
