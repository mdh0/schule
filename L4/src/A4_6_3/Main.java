package A4_6_3;

public class Main {
    public static void main(String[] args) {
        Mietimmobilien mietImmobilie = new Mietimmobilien(new Adresse("Blauer-Stein-Strasse 4", 78176, "Blumberg"), 72, 450, 150);
        System.out.println("Ich wohne in der " + mietImmobilie.adresse.getStrasse() + ", " + mietImmobilie.adresse.getPlz() + " "
                            + mietImmobilie.adresse.getOrt() + ". Ich zahle " + (mietImmobilie.getMonatsMiete()+ mietImmobilie.getNebenKosten()
                            + "€ Miete."));
    }
}
