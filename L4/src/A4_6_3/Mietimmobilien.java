package A4_6_3;

public class Mietimmobilien extends Immobilie {
    private int monatsMiete;
    private int nebenKosten;

    public Mietimmobilien(Adresse adresse, int wohnFlaeche, int monatsMiete, int nebenKosten) {
        super(adresse, wohnFlaeche);
        this.monatsMiete = monatsMiete;
        this.nebenKosten = nebenKosten;
    }

    public int getMonatsMiete() {
        return monatsMiete;
    }

    public void setMonatsMiete(int monatsMiete) {
        this.monatsMiete = monatsMiete;
    }

    public int getNebenKosten() {
        return nebenKosten;
    }

    public void setNebenKosten(int nebenKosten) {
        this.nebenKosten = nebenKosten;
    }

    public int getProvision() {
        return monatsMiete * 2;
    }
}
