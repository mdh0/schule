package A4_5_1;

public class SpeziellerRechner extends Rechner {
    private static double rabatt;

    public SpeziellerRechner(double price, int articleNumber) {
        super(price, articleNumber);
    }

    public static double getRabatt() {
        return rabatt;
    }

    public static void setRabatt(double rabatt) {
        SpeziellerRechner.rabatt = rabatt;
    }

    @Override
    public double getSellingPrice() {
        return getPrice() * (1 - (getRabatt() / 100));
    }
}
