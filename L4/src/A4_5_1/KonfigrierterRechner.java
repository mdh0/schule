package A4_5_1;

public class KonfigrierterRechner extends Rechner {
    private double zuschlag;
    private int lieferZeit;

    public KonfigrierterRechner(double price, int articleNumber, double zuschlag, int lieferZeit) {
        super(price, articleNumber);
        this.zuschlag = zuschlag;
        this.lieferZeit = lieferZeit;
    }

    public KonfigrierterRechner(double price, int articleNumber, double zuschlag) {
        super(price, articleNumber);
        this.zuschlag = zuschlag;
        lieferZeit = 0;
    }

    public double getZuschlag() {
        return zuschlag;
    }

    public int getLieferZeit() {
        return lieferZeit;
    }

    @Override
    public double getSellingPrice() {
        return getPrice() * (1 - (getZuschlag()/100));
    }
}
