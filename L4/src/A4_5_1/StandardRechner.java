package A4_5_1;

public class StandardRechner extends Rechner {
    public StandardRechner(double price, int articleNumber) {
        super(price, articleNumber);
    }

    @Override
    public double getSellingPrice() {
        return this.getPrice();
    }
}
