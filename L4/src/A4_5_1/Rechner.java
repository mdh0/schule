package A4_5_1;

public abstract class Rechner {
    protected double price;
    protected int articleNumber;

    public Rechner(double price, int articleNumber) {
        this.price = price;
        this.articleNumber = articleNumber;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getArticleNumber() {
        return articleNumber;
    }

    public abstract double getSellingPrice();
}
