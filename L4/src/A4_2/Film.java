package A4_2;

public class Film extends Medium {
    private int dauer;
    private FSK fsk;

    public Film(String titel, int anschaffungsJahr, int anschaffungsKosten, Standort standort, int dauer, FSK fsk) {
        super(titel, anschaffungsJahr, anschaffungsKosten, standort);
        this.dauer = dauer;
        this.fsk = fsk;
    }

    public int getDauer() {
        return dauer;
    }

    public FSK getFsk() {
        return fsk;
    }
}
