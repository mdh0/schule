package A4_2;

public abstract class Medium {
    protected String titel;
    protected int anschaffungsJahr;
    protected int anschaffungsKosten;

    public Medium(String titel, int anschaffungsJahr, int anschaffungsKosten, Standort standort) {
        this.titel = titel;
        this.anschaffungsJahr = anschaffungsJahr;
        this.anschaffungsKosten = anschaffungsKosten;
        this.standort = standort;
    }

    protected Standort standort;

    public String getTitel() {
        return titel;
    }

    public int getAnschaffungsJahr() {
        return anschaffungsJahr;
    }

    public int getAnschaffungsKosten() {
        return anschaffungsKosten;
    }

    public String getStandort() {
        return standort.toString();
    }

    public void setStandort(Standort standort) {
        this.standort = standort;
    }
}
