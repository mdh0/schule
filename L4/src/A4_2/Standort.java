package A4_2;

public class Standort {
    private String sachgebiet;
    private int nummer;

    public String getSachgebiet() {
        return sachgebiet;
    }

    public int getNummer() {
        return nummer;
    }

    public Standort(String sachgebiet, int nummer) {
        this.sachgebiet = sachgebiet;
        this.nummer = nummer;
    }

    @Override
    public String toString() {
        return getSachgebiet() + "-" + getNummer();
    }
}
