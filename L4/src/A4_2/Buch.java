package A4_2;

public class Buch extends Medium {
    private String autor;

    public Buch(String titel, int anschaffungsJahr, int anschaffungsKosten, Standort standort, String autor) {
        super(titel, anschaffungsJahr, anschaffungsKosten, standort);
        this.autor = autor;
    }

    public String getAutor() {
        return autor;
    }
}
