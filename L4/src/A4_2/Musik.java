package A4_2;

public class Musik extends Medium {
    private int dauer;

    public Musik(String titel, int anschaffungsJahr, int anschaffungsKosten, Standort standort, int dauer) {
        super(titel, anschaffungsJahr, anschaffungsKosten, standort);
        this.dauer = dauer;
    }

    public int getDauer() {
        return dauer;
    }
}
