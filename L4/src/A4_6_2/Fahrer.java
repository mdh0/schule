package A4_6_2;

public class Fahrer {
    private Fahrzeug fahrzeug;
    private String nachname;
    private String vorname;

    public Fahrer(Fahrzeug fahrzeug, String nachname, String vorname) {
        this.fahrzeug = fahrzeug;
        this.nachname = nachname;
        this.vorname = vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public String getVorname() {
        return vorname;
    }

    public Fahrzeug getFahrzeug() {
        return fahrzeug;
    }

    public void setFahrzeug(Fahrzeug fahrzeug) {
        this.fahrzeug = fahrzeug;
    }
}
