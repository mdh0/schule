package A4_6_2;

public class Fahrzeug {
    private String kennzeichen;
    private double leihgebuehr;
    private int kilometerStand;

    public Fahrzeug(String kennzeichen, double leihgebuehr, int kilometerStand) {
        this.kennzeichen = kennzeichen;
        this.leihgebuehr = leihgebuehr;
        this.kilometerStand = kilometerStand;
    }

    public String getKennzeichen() {
        return kennzeichen;
    }

    public void setKennzeichen(String kennzeichen) {
        this.kennzeichen = kennzeichen;
    }

    public double getLeihgebuehr() {
        return leihgebuehr;
    }

    public void setLeihgebuehr(double leihgebuehr) {
        this.leihgebuehr = leihgebuehr;
    }

    public int getKilometerStand() {
        return kilometerStand;
    }

    public void setKilometerStand(int kilometerStand) {
        this.kilometerStand = kilometerStand;
    }
}
