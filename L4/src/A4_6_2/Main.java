package A4_6_2;

public class Main {
    public static void main(String[] args) {
        Fahrzeug testTaxi = new Fahrzeug("VS-MI-654", 390, 647320);
        Fahrer testFahrer = new Fahrer(testTaxi, "Fischer", "Klaus");
        System.out.println(testFahrer.getFahrzeug().getKennzeichen());
    }
}
