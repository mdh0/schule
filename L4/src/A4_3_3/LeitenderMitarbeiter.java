package A4_3_3;

public class LeitenderMitarbeiter extends Personal {
    private int gehalt;
    private int leistungsPraemie;

    public LeitenderMitarbeiter(String name, int geburtsjahr, int gehalt, int leistungsPraemie) {
        super(name, geburtsjahr);
        this.gehalt = gehalt;
        this.leistungsPraemie = leistungsPraemie;
    }

    public int getGehalt() {
        return gehalt;
    }

    public void setGehalt(int gehalt) {
        this.gehalt = gehalt;
    }

    public int getLeistungsPraemie() {
        return leistungsPraemie;
    }

    public void setLeistungsPraemie(int leistungsPraemie) {
        this.leistungsPraemie = leistungsPraemie;
    }
}
