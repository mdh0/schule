package A4_3_3;

public abstract class Personal {
    protected String name;
    protected int geburtsjahr;

    public Personal(String name, int geburtsjahr) {
        this.name = name;
        this.geburtsjahr = geburtsjahr;
    }

    public String getName() {
        return name;
    }

    public int getGeburtsjahr() {
        return geburtsjahr;
    }
}
