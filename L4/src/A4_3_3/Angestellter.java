package A4_3_3;

public class Angestellter extends Personal {
    private int gehalt;

    public Angestellter(String name, int geburtsjahr, int gehalt) {
        super(name, geburtsjahr);
        this.gehalt = gehalt;
    }

    public int getGehalt() {
        return gehalt;
    }

    public void setGehalt(int gehalt) {
        this.gehalt = gehalt;
    }
}
