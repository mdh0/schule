package A4_3_3;

public class Arbeiter extends Personal {
    private double stundenLohn;
    private int stunden;

    public Arbeiter(String name, int geburtsjahr, double stundenLohn, int stunden) {
        super(name, geburtsjahr);
        this.stundenLohn = stundenLohn;
        this.stunden = stunden;
    }

    public double getStundenLohn() {
        return stundenLohn;
    }

    public int getStunden() {
        return stunden;
    }

    public void setStundenLohn(double stundenLohn) {
        this.stundenLohn = stundenLohn;
    }
}
