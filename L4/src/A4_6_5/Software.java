package A4_6_5;

public class Software extends Artikel {
    private String lizenzTyp;
    private String systemVorraussetzung;

    public Software(int articleNumber, String bezeichnung, double einstandsPreis, String lizenzTyp, String systemVorraussetzung) {
        super(articleNumber, bezeichnung, einstandsPreis);
        this.lizenzTyp = lizenzTyp;
        this.systemVorraussetzung = systemVorraussetzung;
    }

    public String getLizenzTyp() {
        return lizenzTyp;
    }

    public void setLizenzTyp(String lizenzTyp) {
        this.lizenzTyp = lizenzTyp;
    }

    public String getSystemVorraussetzung() {
        return systemVorraussetzung;
    }

    public void setSystemVorraussetzung(String systemVorraussetzung) {
        this.systemVorraussetzung = systemVorraussetzung;
    }
}
