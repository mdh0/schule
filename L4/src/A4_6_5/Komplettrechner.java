package A4_6_5;

public class Komplettrechner extends Artikel {
    private String gehaeuseForm;

    public Komplettrechner(int articleNumber, String bezeichnung, double einstandsPreis, String gehaeuseForm) {
        super(articleNumber, bezeichnung, einstandsPreis);
        this.gehaeuseForm = gehaeuseForm;
    }

    public String getGehaeuseForm() {
        return gehaeuseForm;
    }

    public void setGehaeuseForm(String gehaeuseForm) {
        this.gehaeuseForm = gehaeuseForm;
    }
}
