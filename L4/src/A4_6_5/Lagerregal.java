package A4_6_5;

import java.util.ArrayList;

public class Lagerregal {
    private static int volumen;
    public final String raum;
    public final int nummer;
    ArrayList<Artikel> artikelListe = new ArrayList<>();

    public Lagerregal(String raum, int nummer) {
        this.raum = raum;
        this.nummer = nummer;
    }

    public static int getVolumen() {
        return volumen;
    }

    public static void setVolumen(int volumen) {
        Lagerregal.volumen = volumen;
    }

    public ArrayList<Artikel> getArtikelListe() {
        return artikelListe;
    }
}
