package A4_6_5;

public class Main {
    public static void main(String[] args) {
        Lagerregal.setVolumen(10000);
        Lagerregal regal1 = new Lagerregal("E103", 22);
        Komplettrechner komplettrechner1 = new Komplettrechner(1, "Nice", 3000, "Kreis");
        regal1.getArtikelListe().add(komplettrechner1);
        regal1.getArtikelListe().get(0).einlagern(4);
        regal1.getArtikelListe().get(0).entnehmen(1);
        System.out.println("Komplettrechner Nummer: " + regal1.getArtikelListe().get(0).getArticleNumber() + "; Raum: " +
                regal1.raum);
        System.out.println("Lagerbestand: " + regal1.getArtikelListe().get(0).getLagerBestand() + "; Einstandspreis: " +
                regal1.getArtikelListe().get(0).getEinstandsPreis() + "; Lagerwert: " +
                regal1.getArtikelListe().get(0).berechneLagerWert());
        regal1.getArtikelListe().get(0).entnehmen(5);
    }
}
