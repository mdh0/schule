package A4_6_5;

public abstract class Artikel {
    private int articleNumber;
    private String bezeichnung;
    private double einstandsPreis;
    private int lagerBestand;

    public Artikel(int articleNumber, String bezeichnung, double einstandsPreis) {
        this.articleNumber = articleNumber;
        this.bezeichnung = bezeichnung;
        this.einstandsPreis = einstandsPreis;
        this.lagerBestand = 0;
    }

    public void setEinstandsPreis(double einstandsPreis) {
        this.einstandsPreis = einstandsPreis;
    }

    public int getArticleNumber() {
        return articleNumber;
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

    public double getEinstandsPreis() {
        return einstandsPreis;
    }

    public int getLagerBestand() {
        return lagerBestand;
    }

    public double berechneLagerWert() {
        return lagerBestand * einstandsPreis;
    }

    public void einlagern(int menge) {
        lagerBestand += menge;
    }

    public void entnehmen(int menge) throws IllegalArgumentException {
        if (lagerBestand - menge < 0) {
            throw new IllegalArgumentException("Die angegebene Menge kann nicht entnommen werden.");
        } else {
            lagerBestand -= menge;
        }
    }
}
