package A4_6_5;

public class Einzelkomponenten extends Artikel {
    private String komponentenArt;
    private String anleitung;

    public Einzelkomponenten(int articleNumber, String bezeichnung, double einstandsPreis, String komponentenArt, String anleitung) {
        super(articleNumber, bezeichnung, einstandsPreis);
        this.komponentenArt = komponentenArt;
        this.anleitung = anleitung;
    }

    public String getKomponentenArt() {
        return komponentenArt;
    }

    public String getAnleitung() {
        return anleitung;
    }

    public void setAnleitung(String anleitung) {
        this.anleitung = anleitung;
    }
}
