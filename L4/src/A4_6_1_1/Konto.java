package A4_6_1_1;

public class Konto {
    private double kontoStand;

    public void einzahlen (double einzahlungsBetrag) {
        kontoStand += einzahlungsBetrag;
    }

    public double getKontoStand() {
        return kontoStand;
    }
}
