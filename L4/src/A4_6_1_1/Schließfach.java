package A4_6_1_1;

public class Schließfach {
    private static int schließfachnummer = 0;
    private int nummer;
    private static double miete = 0;
    private Kunde kunde;

    public Schließfach(Kunde kunde) {
        schließfachnummer++;
        nummer = schließfachnummer;
        this.kunde = kunde;
    }

    public int getNummer() {
        return nummer;
    }

    public double getMiete() {
        return miete;
    }

    public void setMiete(double miete) {
        Schließfach.miete = miete;
    }

    public Kunde getKunde() {
        return kunde;
    }

    public void setKunde(Kunde kunde) {
        this.kunde = kunde;
    }
}
