package A4_6_1_1;

public class Main {
    public static void main(String[] args) {
        Konto testKonto = new Konto();
        testKonto.einzahlen(3500);
        testKonto.einzahlen(1973.47);
        Kunde testKunde = new Kunde("Steffi", testKonto);
        System.out.println(testKunde.getName());

        System.out.println(testKunde.getKonto().getKontoStand());
    }
}
