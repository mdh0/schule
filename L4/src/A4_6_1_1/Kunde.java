package A4_6_1_1;

public class Kunde {
    private String name;

    private Konto meinKonto;

    private Schließfach meinSchließfach;

    public Kunde(String name, Konto meinKonto) {
        this.name = name;
        this.meinKonto = meinKonto;
        this.meinSchließfach = new Schließfach(this);
    }

    public void setName (String neuerName) {
        name = neuerName;
    }

    public String getName() {
        return name;
    }

    public void setKonto (Konto neuesKonto) {
        meinKonto = neuesKonto;
    }

    public Konto getKonto() {
        return meinKonto;
    }
}
