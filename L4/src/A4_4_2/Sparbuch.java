package A4_4_2;

public class Sparbuch {
    private String id;
    private double kontostand;
    private static double zinssatz = 0;

    public static double getZinssatz() {
        return zinssatz;
    }

    public Sparbuch(String id) {
        this.id = id;
        this.kontostand = 0;
    }

    public double getKontostand() {
        return kontostand;
    }

    public static void setZinssatz(double zinssatz) {
        Sparbuch.zinssatz = zinssatz;
    }

    public void addToKontostand(double moneytoAdd) {
        this.kontostand += moneytoAdd;
    }

    public void addZinsen() {
        this.kontostand *= (1 + (zinssatz/100));
    }
}
