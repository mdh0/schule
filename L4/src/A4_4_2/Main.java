package A4_4_2;

public class Main {
    public static void main(String[] args) {
        Sparbuch testSparbuch = new Sparbuch("76241");
        testSparbuch.addToKontostand(1900);
        Sparbuch.setZinssatz(1.75);
        System.out.println(testSparbuch.getKontostand() + ";" + Sparbuch.getZinssatz());
    }
}
