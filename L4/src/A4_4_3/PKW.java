package A4_4_3;

public class PKW {
    private double anschaffungsPreis;
    private String kennzeichen;
    private static int abschreibungsdauer;

    public PKW(double anschaffungsPreis, String kennzeichen) {
        this.anschaffungsPreis = anschaffungsPreis;
        this.kennzeichen = kennzeichen;
    }

    public void setKennzeichen(String kennzeichen) {
        this.kennzeichen = kennzeichen;
    }

    public static void setAbschreibungsdauer(int abschreibungsdauer) {
        PKW.abschreibungsdauer = abschreibungsdauer;
    }

    public double getAnschaffungsPreis() {
        return anschaffungsPreis;
    }

    public String getKennzeichen() {
        return kennzeichen;
    }

    public static int getAbschreibungsdauer() {
        return abschreibungsdauer;
    }

    public double berechneAbschreibungProJahr() {
        return getAnschaffungsPreis() / getAbschreibungsdauer();
    }
}
