package A4_4_3;

public class Main {
    public static void main(String[] args) {
        PKW.setAbschreibungsdauer(10);
        PKW testPkw = new PKW(39850, "FR-EF-54");
        System.out.println("Anschaffungspreis: " + testPkw.getAnschaffungsPreis() + ";Abschreibungsdauer: " + PKW.getAbschreibungsdauer());
    }
}
