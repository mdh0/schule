package A4_1;

public class Tour {
    private String name;
    private String kaptiaen;
    private String schiffsName;

    public Tour(String name, String kaptiaen, String schiffsName) {
        this.name = name;
        this.kaptiaen = kaptiaen;
        this.schiffsName = schiffsName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKaptiaen() {
        return kaptiaen;
    }

    public void setKaptiaen(String kaptiaen) {
        this.kaptiaen = kaptiaen;
    }

    public String getSchiffsName() {
        return schiffsName;
    }

    public void setSchiffsName(String schiffsName) {
        this.schiffsName = schiffsName;
    }
}
