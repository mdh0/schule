package A4_1;

public class MehrTagigeTour extends Tour {
    private int dauer;
    private String zielHafen;

    public MehrTagigeTour(String name, String kaptiaen, String schiffsName, int dauer, String zielHafen) {
        super(name, kaptiaen, schiffsName);
        this.dauer = dauer;
        this.zielHafen = zielHafen;
    }

    public int getDauer() {
        return dauer;
    }

    public void setDauer(int dauer) {
        this.dauer = dauer;
    }

    public String getZielHafen() {
        return zielHafen;
    }

    public void setZielHafen(String zielHafen) {
        this.zielHafen = zielHafen;
    }
}
