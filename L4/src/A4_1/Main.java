package A4_1;

public class Main {
    public static void main(String[] args) {
        Tour tour1 = new Tour("Geile Tour", "Krasser Kaptitän", "Noch krasseres Schiff");
        System.out.println("Die Tour " + tour1.getName() + " mit dem Kapitän " + tour1.getKaptiaen()
                            + " findet mit dem Schiff " + tour1.getSchiffsName() + " statt.");
    }
}
